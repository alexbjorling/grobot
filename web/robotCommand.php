<?php
    error_reporting(E_ALL);

    $request = '';
    /* Parse the (command line or browser) arguments */
    if (PHP_SAPI === 'cli') {
        $request = $argv[1];
    }
    else {
        $request = $_GET['command'];
    }

    /* Create a TCP/IP socket. */
    $port = 6666;
    $address = gethostname();
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
    }

    /* Connect */
    $result = socket_connect($socket, $address, $port);
    if ($result === false) {
        echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
    }

    /* Send request and get reply */
    $out = '';
    socket_write($socket, $request, strlen($request));
    while ($out = socket_read($socket, 2048)) {
        echo $out;
    }

    /* Close the socket */
    socket_close($socket);

?>

