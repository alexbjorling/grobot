import time, datetime, os

class Logger():
    
    def __init__(self, filename, info=True, debug=False, warnings=True):
        self.do_info = info
        self.do_debug = debug
        self.do_warnings = warnings

        # for relative paths, prepend this code directory
        if filename.strip()[0] != "/":
            filename = os.path.dirname(os.path.realpath(__file__)) + "/" + filename
        self.filename = filename
        self.info("========= Opening log =========")
    
    def info(self, message):
        if not self.do_info:
            return
        fp = open(self.filename, 'a')
        timeStamp = time.time()
        timeString = datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d %H:%M:%S') 
        fp.write(timeString + " " + message + "\n")
        fp.close()
        
    def debug(self, message):
        if not self.do_debug:
            return
        fp = open(self.filename, 'a')
        timeStamp = time.time()
        timeString = datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d %H:%M:%S') 
        fp.write(timeString + " DEBUG: " + message + "\n")
        fp.close()
        
    def warning(self, message):
        if not self.do_warnings:
            return
        fp = open(self.filename, 'a')
        timeStamp = time.time()
        timeString = datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d %H:%M:%S') 
        fp.write(timeString + " WARNING: " + message + "\n")
        fp.close()
        
