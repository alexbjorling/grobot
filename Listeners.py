import threading
import socket
import time
import serial

class Listener(threading.Thread):
    def __init__(self, buff):
        self.buffer = buff

class SerialListener(Listener):
    def __init__(self, port, buff, log):
        threading.Thread.__init__(self)
        self.serial = serial.Serial(port)
        # call the base class constructor
        super(SerialListener, self).__init__(buff)
        
    def run(self):
        while True:
            if self.serial.read() == 'R':
                self.buffer[0] = int(self.serial.read(4))
            

class TcpListener(Listener):
    
    def __init__(self, host, port, buff, log):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port
        self.log = log
        self.serverSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM) # sock_stream means connection-based (so, tcp)
        self.serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serverSocket.bind((host,port))
        self.serverSocket.listen(3) # make this a "server socket", basically a switchboard.
        # call the base class constructor
        super(TcpListener, self).__init__(buff)
        
    def run(self):
        while True:
            clientSocket, addr = self.serverSocket.accept() 
            self.log.debug("connected a client")
            request = clientSocket.recv(1024).strip()
            self.buffer.setRequest(request)
            self.log.debug("Listener received and saved a command: '" + request + "'")
            while not self.buffer.responseWaiting():
                time.sleep(.1)
            clientSocket.send(self.buffer.getResponse())
            clientSocket.close()

class Buffer():
    def __init__(self):
        self.request = ''
        self.response = ''
        
    def getRequest(self):
        tmp = self.request
        self.request = ''
        return tmp
        
    def getResponse(self):
        tmp = self.response
        self.response = ''
        return tmp
        
    def setRequest(self, msg):
        self.request = msg
        
    def setResponse(self, msg):
        self.response = msg
        
    def requestWaiting(self):
        return bool(len(self.request))
        
    def responseWaiting(self):
        return bool(len(self.response))
