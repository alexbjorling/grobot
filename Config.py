class Config():
    
    def __init__(self, filename):
        
        # open the file with error handling
        try:
            fp = open(filename, 'r')
        except:
            raise Exception("grobotDaemon failed to open configuration file.")

        # go through the file line by line
        for line in fp:
            
            # skip empty lines
            try:
                command = line.split()[0]
            except IndexError:
                continue
            
            # look for directives
            if strCmp(command, "RightMotorPWM"):
                try:
                    self.rightFwd, self.rightStill, self.rightBwd = map(int, line.strip().split()[1:])
                except:
                    raiseIncomplete()
            
            elif strCmp(command, "LeftMotorPWM"):
                try:
                    self.leftFwd, self.leftStill, self.leftBwd = map(int, line.strip().split()[1:])
                except:
                    raiseIncomplete()
                
            elif strCmp(command, "PWMAddress"):
                self.PWMAddress = int(line.strip().split()[1], 16)
                
            elif strCmp(command, "PWMFrequency"):
                self.PWMFrequency = int(line.strip().split()[1])
                
            elif strCmp(command, "LaserPin"):
                self.laserPin = int(line.strip().split()[1])

            elif strCmp(command, "LogFile"):
                self.logFile = line.split()[1].strip()
                
            elif strCmp(command, "CalibrationFile"):
                self.calibrationFile = line.split()[1].strip()
                
            elif strCmp(command, "laserCenter"):
                self.laserCenter = map(int, line.strip().split()[1:])
                
            elif strCmp(command, "FeederPWM"):
                try:
                    self.feederFwd, self.feederStill, self.feederBwd = map(int, line.strip().split()[1:])
                except:
                    raiseIncomplete()

            elif strCmp(command, 'i2cDevice'):
                self.i2cDevice = int(line.strip().split()[1])

            elif strCmp(command, 'CamImage'):
                self.camImage = line.split()[1].strip()

        fp.close()
        
        # check that the configuration is complete
        try:
            self.logFile
            self.rightFwd
            self.rightStill
            self.rightBwd
            self.leftFwd
            self.leftStill
            self.leftBwd
            self.PWMAddress
            self.PWMFrequency
            self.laserPin
            self.calibrationFile
            self.laserCenter
            self.feederFwd
            self.feederStill
            self.feederBwd
        except:
            raiseIncomplete()

def strCmp(str1, str2):
    return (str1.lower() == str2.lower())

def raiseIncomplete():
    raise Exception("grobotDaemon configuration file incomplete or badly formatted.")
