import numpy as np
import scipy.interpolate
import time, os
import matplotlib.pyplot as plt

class Calibration():

    def __init__(self, config, pwm, log):

        # Load a calibration if it exists
        path = os.path.dirname(os.path.realpath(__file__)) + "/"
        try:
            filename = path + config.calibrationFile
            coords = np.loadtxt(filename, usecols=[0, 1])
            positions  = np.loadtxt(filename, usecols=[2, 3])
            log.info("Loaded existing laser calibration")
        except IOError:
            self.recalibrate(config, pwm, log)
            coords = np.loadtxt(filename, usecols=[0, 1])
            positions  = np.loadtxt(filename, usecols=[2, 3])
            log.info("Loaded new laser calibration")

        self.laserPositionA = scipy.interpolate.interp2d(coords[:,0], coords[:,1], positions[:,0], kind='linear')
        self.laserPositionB = scipy.interpolate.interp2d(coords[:,0], coords[:,1], positions[:,1], kind='linear')
        
    def laserPosition(self, x, y):
        return (int(self.laserPositionA(x, y)), int(self.laserPositionB(x, y)))
        
    def recalibrate(self, config, pwm, log):

        def moveLaser(pwm, a, b):
                pwm.setPWM(3, 0, int(a))
                pwm.setPWM(2, 0, int(b))
                time.sleep(.5)
        
        def getImage():
            return np.mean(plt.imread("/dev/shm/mjpeg/cam.jpg"), axis=2)
        
        def getMaxMin(diff):
            xymax = np.where(diff == np.max(diff))
            xymax = [xymax[0][0], xymax[1][0]]
            xymax[0] = diff.shape[0] - xymax[0]
            xymax = np.array([xymax[1], xymax[0]])
            xymin = np.where(diff == np.min(diff))
            xymin = [xymin[0][0], xymin[1][0]]
            xymin[0] = diff.shape[0] - xymin[0]
            xymin = np.array([xymin[1], xymin[0]])
            return xymax, xymin

        log.info("Recalibrating laser")
        moveLaser(pwm, *config.laserCenter)
        
        # temporary: just write some good values
        if 0:
            ul = [462, 460]
            ur = [355, 455]
            ll = [455, 380]
            lr = [353, 373]
            coords = np.array([[0,0], [510,0], [510,382], [0,382]], dtype=int)
            positions = np.array([ul, ur, lr, ll], dtype=int)
            np.savetxt('grobot.cal', np.hstack((coords, positions)), fmt='%d')
            
        # iteratively search for the corners
        if 1:
            ### Initial parameters
            tmp = getImage()
            coords = [
                      [0,0], 
                      [tmp.shape[1], 0], 
                      [tmp.shape[1], tmp.shape[0]], 
                      [0, tmp.shape[0]],
                      #[0, int(tmp.shape[0]/2)],
                      #[int(tmp.shape[1]/2), int(tmp.shape[0]/2)],
                      #[int(tmp.shape[1]), int(tmp.shape[0]/2)],
                      ]
            pos0 = config.laserCenter
            delta = 20
            
            ### Determine a and b vectors
            moveLaser(pwm, *pos0)
            im1 = getImage()
            moveLaser(pwm, pos0[0]+delta, pos0[1])
            im2 = getImage()
            moveLaser(pwm, pos0[0], pos0[1]+delta)
            im3 = getImage()
            v1, v0 = getMaxMin(im2-im1)
            v2, vx = getMaxMin(im3-im2)
            a = (v1-v0) / delta
            b = (v2-v0) /delta
            
            positions = []
            for target in coords:
                ### Find the target iteratively
                # current position and the matrix ab=[a b]
                xy = v0
                pos = pos0
                ab = np.vstack((a, b)).T
                for i in range(50):
                    # aim for a vector half way to the target
                    v = .4 * (target-xy)
                    x = np.linalg.solve(ab, v)
                    moveLaser(pwm, pos[0]+x[0], pos[1]+x[1])
                    pos = [pos[0]+x[0], pos[1]+x[1]]
                    im2 = getImage()
                    if (np.max(im2-im1) < 50) or (np.linalg.norm(x) < 1):
                        positions += [map(int, pos)]
                        break
                    xy = getMaxMin(im2-im1)[0]
            moveLaser(pwm, *pos0)
            np.savetxt('grobot.cal', np.hstack((coords, positions)), fmt='%d')

