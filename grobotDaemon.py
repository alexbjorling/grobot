#!/usr/bin/env python

import sys, time, os
import numpy as np
import random
from Daemon import Daemon
from Listeners import SerialListener, TcpListener, Buffer
#from SerialListener import SerialListener
from Logger import Logger
from Config import Config
from Calibration import Calibration
from PWM.Adafruit_PWM_Servo_Driver import PWM
import RPi.GPIO as gpio
import smbus
from LaserFinder import filteredLaserPosition, simpleLaserPosition

# The RoboDaemon class inherits Daemon, which I took from the internet.
class RoboDaemon(Daemon):
    
    def __init__(self, pidfile, configfile=None, stdout="/dev/null", stderr="/dev/null"):
        if configfile:
            # set up a configuration:
            self.config = Config(filename=configfile)
            # set up a log handler:
            self.log = Logger(filename=self.config.logFile, debug=True)
        # call the base class constructor
        super(RoboDaemon, self).__init__(pidfile=pidfile, stdout=stdout, stderr=stderr)
    
    def run(self):
        
        # set up a buffer for network commands and responses, create and start a listening thread
        tcpBuffer = Buffer()
        self.tcpListener = TcpListener('', 6666, tcpBuffer, self.log)
        self.tcpListener.start()
        
        # set up a buffer for incoming serial communication
        serialBuffer = [0]
        self.serialListener = SerialListener('/dev/ttyAMA0', serialBuffer, self.log)
        self.serialListener.start()
        
        # store the laser offset and status
        self.laserOffsetX = int(514.0/2)
        self.laserOffsetY = int(384.0/2)
        self.laserOn = False
        self.laserMode = 'point'

        # set up the hardware
        self.setupGpio()
        self.setupI2C()
        self.setupPwm()
        self.setupLeds()
        self.calibration = Calibration(self.config, self.pwm, self.log)
        self.moveMotors('stop')
        self.setupLaser()

        # the main loop
        AUTO, MANUAL = range(2)
        state = MANUAL
        while True:
            time.sleep(.050)
            
            if state == AUTO:
                if tcpBuffer.requestWaiting():
                    command = tcpBuffer.getRequest()
                    if command == 'manual':
                        self.log.info("Going into manual mode")
                        state = MANUAL
                        tcpBuffer.setResponse('ok')
                        self.moveMotors('stop')
                        continue
                    else:
                        tcpBuffer.setResponse('in auto mode!')

                # do other AUTO things, such as looking for cats. this is a trivial example with a distance sensor:
                distance = serialBuffer[0]
                try: 
                    oldDistance
                except: 
                    oldDistance = distance
                if (oldDistance > 300) & (distance == 300):
                    action = ['left', 'right'][random.randint(0,1)]
                    self.moveMotors(action)
                elif distance > 300:
                    action = 'forward'
                    self.moveMotors(action)
                oldDistance = distance
                    
            elif state == MANUAL:
                if tcpBuffer.requestWaiting():
                    command = tcpBuffer.getRequest()
                    if command in ['forward', 'backward', 'stop', 'right', 'left']:
                        self.moveMotors(command)
                        tcpBuffer.setResponse('ok')
                    elif command == 'auto':
                        self.log.info("Going into auto mode")
                        state = AUTO
                        tcpBuffer.setResponse('ok')
                    elif (command.split()[0] == 'click') & (self.laserOn):
                        self.laserOffsetX = int(command.split()[1])
                        self.laserOffsetY = int(command.split()[2])
                        #self.moveLaser(self.laserOffsetX, self.laserOffsetY)
                        tcpBuffer.setResponse('ok')
                    elif command == 'laserPoint':
                        self.laserMode = 'point'
                        tcpBuffer.setResponse('ok')
                    elif command == 'laserCircle':
                        self.laserMode = 'circle'
                        tcpBuffer.setResponse('ok')
                    elif command == 'laserBox':
                        self.laserMode = 'box'
                        tcpBuffer.setResponse('ok')
                    elif command == 'laserOff':
                        self.laserOn = False
                        tcpBuffer.setResponse('ok')
                    elif command == 'laserOn':
                        self.laserOn = True
                        tcpBuffer.setResponse('ok')
                    elif command == 'reCalibrate':
                        path = os.path.dirname(os.path.realpath(__file__)) + "/"
                        os.remove(path + self.config.calibrationFile)
                        self.calibration = Calibration(self.config, self.pwm, self.log)
                        tcpBuffer.setResponse('ok')
                    elif command == 'feed':
                        self.feed()
                        tcpBuffer.setResponse('ok')
                    elif command == 'ledsOff':
                        self.ledsOff()
                        tcpBuffer.setResponse('ok')
                    elif command == 'ledsWhite':
                        self.ledsWhite()
                        tcpBuffer.setResponse('ok')
                    elif command == 'ledsIr':
                        self.ledsIr()
                        tcpBuffer.setResponse('ok')
                    else:
                        tcpBuffer.setResponse('bad command')

                if self.laserOn:
                    position = simpleLaserPosition(self.config.camImage)
                    delta1 = int(round(.5*(self.laserOffsetY - position[0])))
                    delta0 = -int(round(.5*(self.laserOffsetX - position[1])))
                    self.laserPwm0 += delta0
                    self.laserPwm1 += delta1
                    print 'target: ', [self.laserOffsetY, self.laserOffsetX], 'measured: ', position, 'change PWM by: ', delta0, delta1
                    self.pwm.setPWM(3, 0, self.laserPwm0)
                    self.pwm.setPWM(2, 0, self.laserPwm1)
                    time.sleep(.1)

                # set laser status
                self.toggleLaser(self.laserOn)

                # do some laser figure
                if self.laserOn & (self.laserMode == 'circle'):
                    t = time.time()
                    angle = ((t - int(t)) * 2 * np.pi) * 4
                    self.moveLaser(self.laserOffsetX + 51*np.cos(angle), self.laserOffsetY + 38*np.sin(angle))
                if self.laserOn & (self.laserMode == 'box'):
                    dx, dy = [[-1, -1], [-1, 1], [1, 1], [1, -1]][int(time.time()*10)%4]
                    self.moveLaser(self.laserOffsetX + 50*dx, self.laserOffsetY + 40*dy)
                    
        tcpListener.join()
        serialListener.join()

    def setupLaser(self):
        self.laserPwm0 = self.config.laserCenter[0]
        self.laserPwm1 = self.config.laserCenter[1]
        self.pwm.setPWM(3, 0, self.laserPwm0)
        self.pwm.setPWM(2, 0, self.laserPwm1)
        
    def setupGpio(self):
        gpio.setwarnings(False)
        gpio.setmode(gpio.BCM)
        gpio.setup(self.config.laserPin, gpio.OUT)

    def setupI2C(self):
        self.log.debug("trying to set up I2C device %d"%self.config.i2cDevice)
        try:
            self.i2cBus = smbus.SMBus(self.config.i2cDevice)
        except IOError:
            msg = "failed to open I2C device"
            self.log.debug(msg)
            raise Exception(msg)

    def setupLeds(self):
        # crank up the white leds
        self.i2cBus.write_byte_data(0x70, 0x02, 0x32)
        self.i2cBus.write_byte_data(0x70, 0x04, 0x32)
        self.i2cBus.write_byte_data(0x70, 0x05, 0x32)
        self.i2cBus.write_byte_data(0x70, 0x07, 0x32)
        # and the IR leds
        self.i2cBus.write_byte_data(0x70, 0x01, 0x32)
        self.i2cBus.write_byte_data(0x70, 0x03, 0x32)
        self.i2cBus.write_byte_data(0x70, 0x06, 0x32)
        self.i2cBus.write_byte_data(0x70, 0x08, 0x32)

    def ledsOff(self):
        self.i2cBus.write_byte_data(0x70, 0x00, 0x00)

    def ledsWhite(self):
        self.i2cBus.write_byte_data(0x70, 0x00, 0x5a)

    def ledsIr(self):
        self.i2cBus.write_byte_data(0x70, 0x00, 0xa5)
            
    def setupPwm(self):
        self.pwm = PWM(self.i2cBus, self.config.PWMAddress)
        self.pwm.setPWMFreq(self.config.PWMFrequency)
        
    # x and y are in pixels, (horizontal, vertical) with the origin top left
    def moveLaser(self, x, y):
        a, b = self.calibration.laserPosition(x, y)
        self.pwm.setPWM(3, 0, a)
        self.pwm.setPWM(2, 0, b)
        
    def toggleLaser(self, level):
        gpio.output(self.config.laserPin, level)
        
    def moveMotors(self, request):
        # stop
        if request == 'stop':
            self.pwm.setPWM(0, 0, self.config.rightStill)
            self.pwm.setPWM(1, 0, self.config.leftStill)
        # forward
        elif request == 'forward':
            self.pwm.setPWM(0, 0, self.config.rightFwd) # right side
            self.pwm.setPWM(1, 0, self.config.leftFwd) # left side
        # backward
        elif request == 'backward':
            self.pwm.setPWM(0, 0, self.config.rightBwd)
            self.pwm.setPWM(1, 0, self.config.leftBwd)
        # left
        elif request == 'right':
            self.pwm.setPWM(0, 0, self.config.rightBwd)
            self.pwm.setPWM(1, 0, self.config.leftFwd)
        # right
        elif request == 'left':
            self.pwm.setPWM(0, 0, self.config.rightFwd)
            self.pwm.setPWM(1, 0, self.config.leftBwd)

    def feed(self):
        self.pwm.setPWM(4, 0, self.config.feederBwd)
        time.sleep(.2)
        self.pwm.setPWM(4, 0, self.config.feederFwd)
        time.sleep(.4)
        self.pwm.setPWM(4, 0, self.config.feederStill)

if __name__ == "__main__":
    # use the config file "grobot.conf" in the directory where this file is:
    configFile = os.path.dirname(os.path.realpath(__file__)) + "/grobot.conf"
    daemon = RoboDaemon(pidfile="/tmp/grobot.pid", configfile=configFile, stdout="/dev/stdout", stderr="/dev/stderr")
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
