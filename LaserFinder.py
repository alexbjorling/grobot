import matplotlib.pyplot as plt
import numpy as np
import cv2

def filteredLaserPosition(imfile, thresholdBlockSize = 15, thresholdOffset = -20, globalThreshold = 200, k = 15, l = 7, filterThreshold = .001, smoothK = 5):
    """ Takes a single image and tries to return the position of the laser 
        pointer. It uses a series of filtering operations for this, and each 
        has its parameters, the default values seem to work ok. """
    
    image = cv2.imread(imfile)[:, :, -1] # BGR
    th = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, thresholdBlockSize, thresholdOffset) * (image > globalThreshold)
    kernel = np.ones((k, k), dtype=np.float)
    kernel[k/2 - l/2: k/2 + l/2 + 1, k/2 - l/2: k/2 + l/2 + 1] = 0
    filtered = cv2.filter2D(th, cv2.CV_32F, kernel)
    filtered = filtered > filterThreshold * filtered.max()
    composite = th * (1 - filtered)
    if composite.max() > 0:
        smoothed = cv2.blur(composite, (smoothK,)*2)
    else:
        smoothed  = cv2.blur(th, (7,7))
    maxInd = np.where(smoothed == np.max(smoothed))
    position = (maxInd[0][0], maxInd[1][0])
    return position

def simpleLaserPosition(imfile):
    im = cv2.imread(imfile)[:, :, 1] # green
    where = np.where(im == im.max())
    return np.array([where[0][0], where[1][0]])
